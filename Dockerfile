FROM cern/cc7-base

MAINTAINER Chris Watson (cwatson@cern.ch) c/o SUPERVISOR Roland Sipos (rsipos@cern.ch)

EXPOSE 8080 8778

# Set the labels that are used for OpenShift to describe the builder image.
LABEL io.k8s.description="Tomcat 6.0.53 Elisa Webserver" \
    io.k8s.display-name="Tomcat6.0.53 Elisa" \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="builder,webserver,html,tomcat6" \
    # this label tells s2i where to find its mandatory scripts
    # (run, assemble, save-artifacts)
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN yum update -y && yum upgrade -y && yum install -y vim iputils tar openldab java-1.7.0-openjdk javamail classpathx-jaf classpathx-mail && yum clean all -y

ADD apache-tomcat-6.0.24.tar.gz /var/lib/
RUN mv /var/lib/apache-tomcat-6.0.24 /var/lib/tomcat6

COPY etc/tomcat6/. /var/lib/tomcat6/conf/.
COPY src/. /var/lib/tomcat6/webapps/.

# Add necessary files from host pc
COPY ./s2i/bin/ /usr/libexec/s2i

RUN cp /var/lib/tomcat6/conf/setenv.sh /var/lib/tomcat6/bin/setenv.sh

RUN groupadd tomcat
RUN useradd -s /bin/bash -g tomcat tomcat
RUN chown -Rf tomcat.root /var/lib/tomcat6
RUN chmod -R 775 /var/lib/tomcat6/webapps
RUN chmod -R 775 /var/lib/tomcat6/conf
RUN chmod -R 775 /var/lib/tomcat6/logs && chmod -R 775 /var/lib/tomcat6/work

# Set default container user to 1001 as required by s2i
USER 1001

CMD ["/usr/libexec/s2i/usage"]

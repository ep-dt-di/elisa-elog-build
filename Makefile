IMAGE_NAME = tomcat6-elisa
#gitlab-registry.cern.ch/rsipos/test-dtdielog/test-dtdielog-elisa:builder

.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: test
test:
	docker build -t $(IMAGE_NAME)-candidate .
	IMAGE_NAME=$(IMAGE_NAME)-candidate test/run
